package com.gitlab.caluml.one;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OneTest {

	private final One one = new One();

	@Test
	public void Leap_years_are_correctly_identified() {
		for (int year : new int[]{1600, 1996, 2000, 2004, 2008, 2400}) {
			assertThat(one.run(year))
				.as(year + " is a leap-year")
				.isTrue();
		}
	}

	@Test
	public void Non_leap_years_are_correctly_identified() {
		for (int year : new int[]{1700, 1800, 1900, 2001, 2002, 2003, 2100}) {
			assertThat(one.run(year))
				.as(year + " is not a leap-year")
				.isFalse();
		}
	}

}
