This project is for you to abuse Java as much as is possible.

Use all your skill and experience as a developer to write the worst possible implementation you can, using all the tricks that make code horrible to follow, but that at the same time pass the tests.

* Create a branch off master
* Look at the tests
* Write the worst, most confusing implementation you can
* Make sure the tests still pass
* Push your branch so we can share your warped mind!

Ideas to help make your code horrible:
* Formatting
* Badly named/misleading variables
* Hard to follow code
* Cyclomatic complexity
* Casting
* Global state, mutated from everywhere
* Lambda abuse
* Reflection

Remember - as long as the tests pass though, it's all good!

Only the tests have any reference to what the code should actually do. This is so that people looking at the implementation don't have any hints as to what it should do. 